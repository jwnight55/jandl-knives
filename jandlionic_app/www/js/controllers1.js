angular.module('jandl.controllers', [])

 

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $cordovaCamera) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
    
    
  

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/imadd.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.imaddform = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeimadd = function() {
    $scope.imaddform.hide();
  };

  // Open the login modal
  $scope.imadd = function() {
    $scope.imaddform.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Uploading Image', $scope.imageData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    
      $scope.closeimadd();
    
      
      
  };
    
    

})

  
.controller('addController', ['$scope', '$location','$state','$stateParams', '$http', function ($scope, $location ,$state, $stateParams, $http) {
    $scope.$on('$ionicView.enter', function(){
    $scope.knife = {
        'name' : '',
        'description': '',
        'image': '',
        'wlink': ''
    };
    if(angular.isDefined($stateParams.id)){
        var $id = $stateParams.id;
        $scope.title = "Edit";
        var $link = 'http://localhost:8080/jandl/scripts/query.php?a=d&id=' + $id;
        
        var $ae = "e";
        
        $http.get($link).success(function(data) {
            $scope.knife = data;
        });
    }else{
        $scope.title = "Add";
        var $ae = "a";
    }
    $scope.doAddEdit = function(){
            $name = $scope.knife.name;
            $description = $scope.knife.description;
            $image = $scope.knife.image;
            $wlink = $scope.knife.wlink;
        if($ae == "a"){
           var $link = 'http://localhost:8080/jandl/scripts/query.php?a=a&n="' + $name + '"&d="' + $description + '"&i="' + $image + '"&l="'+ $wlink+'"';
            $http.get($link);
            
            var $job = "Added";
            
            }else if($ae =="e"){
            var $link ='http://localhost:8080/jandl/scripts/query.php?a=e&n="' + $name + '"&d="' + $description + '"&i="' + $image + '"&l="'+ $wlink+ '"&id="' + $id + '"';
            $http.get($link);
            var $job ="Edited";
        }
        $scope.knife = {
                'name' : '',
                'description': '',
                'image': '',
                'wlink': ''
        };
        var $path = "/app/complete/" + $job;
        $location.path($path);
        };  
    
   
   })      

}])

.controller('editController', ['$scope', '$http', function ($scope, $http) {
    $scope.$on('$ionicView.enter', function(){
        
       $http.get('http://localhost:8080/jandl/scripts/query.php?a=q').success(function(data) {
            $scope.knives = data; })
       
    })
  
     
   
    
}])

.controller('deleteController', ['$scope','$state', '$http', function ($scope, $state, $http) {
    $scope.$on('$ionicView.enter', function(){
        
       $http.get('http://localhost:8080/jandl/scripts/query.php?a=q').success(function(data) {
    $scope.knives = data; })
        
    })
    
    
    
  

}])

.controller('dfirmController', ['$scope','$location','$state' ,'$stateParams','$http', function ($scope, $location,$state, $stateParams,$http){
    var $id = $stateParams.id;
    var $link = 'http://localhost:8080/jandl/scripts/query.php?a=d&id=';
    var $res = $link.concat($id);
              
    $http.get($res).success(function(data) {
        $scope.knife = data;
    });

    $scope.go = function(path) {
    $location.path(path);
    }
    
    $scope.delete = function(path){
        
        var $link = 'http://localhost:8080/jandl/scripts/query.php?a=r&id=';
        var $res = $link.concat($id);
        $http.get($res);
        
        $location.path('/app/complete/Deleted');
        
    }
}])
   
.controller('ddoneController',['$scope','$location', '$state', '$stateParams', function ($scope, $location, $state, $stateParams){
     $scope.title = $stateParams.job; 
    
    $scope.go = function(path) {
   
    $location.path('/app/home');
   
    
    }                           
                               

}]) 

.controller('homeController',['$scope','$location', function ($scope, $location){
    
    $scope.go = function(path){
        $location.path(path)
    }
    
}])
;
