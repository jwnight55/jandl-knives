'use strict';

angular.module('JLApp', ['ui.router','ngResource','ngDialog'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                        controller  : 'HomeController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }

            })
        
            // route for the aboutus page
            .state('app.aboutus', {
                url:'aboutus',
                views: {
                    'content@': {
                        templateUrl : 'views/aboutus.html'
                                         
                    }
                }
            })
        
            // route for the contactus page
            .state('app.contactus', {
                url:'contactus',
                views: {
                    'content@': {
                        templateUrl : 'views/contactus.html'                 
                    }
                }
            })

            // route for the Gallery page
            .state('app.gallery', {
                url: 'gallery',
                views: {
                    'content@': {
                        templateUrl : 'views/gallery.html',
                        controller  : 'GalleryController'
                    }
                }
            })

            // route for the dishdetail page
            .state('app.knifedetails', {
                url: 'gallery/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/knifedetail.html',
                        controller  : 'KnifeDetailController'
                   }
                }
            });
    
        $urlRouterProvider.otherwise('/');
    }) ;l