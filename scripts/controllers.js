'use strict';

angular.module('JLApp')

.controller('GalleryController', ['$scope', '$http', function ($scope, $http) {
    
    $http.get('scripts/query.php?a=q').success(function(data) {
    $scope.knives = data;
    
  });
     
   
}])



.controller('KnifeDetailController', ['$scope', '$state', '$stateParams', '$http', function ($scope, $state, $stateParams, $http) {

    


              var $id =$stateParams.id
              var $link = 'scripts/query.php?a=d&id=';
              var $res = $link.concat($id);
              
              $http.get($res).success(function(data) {
              $scope.knife = data;
              });
    
              
}])

// implement the IndexController and About Controller here

.controller('HomeController', ['$scope', '$http', function ($scope, $http) {
            
              var $id ="x";
              var $link = 'scripts/query.php?a=d&id=';
              var $res = $link.concat($id);
              
              $http.get($res).success(function(data) {
              $scope.knife = data;
              });
}])





.controller('HeaderController', ['$scope', '$state', '$rootScope', function ($scope, $state, $rootScope) {

    $scope.stateis = function(curstate) {
       return $state.is(curstate);  
    };
    
}])
/*
.controller('LoginController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory', function ($scope, ngDialog, $localStorage, AuthFactory) {
    
    $scope.loginData = $localStorage.getObject('userinfo','{}');
    
    $scope.doLogin = function() {
        if($scope.rememberMe)
           $localStorage.storeObject('userinfo',$scope.loginData);

        AuthFactory.login($scope.loginData);

        ngDialog.close();

    };
            
    $scope.openRegister = function () {
        ngDialog.open({ template: 'views/register.html', scope: $scope, className: 'ngdialog-theme-default', controller:"RegisterController" });
    };
    
}])


*/
;